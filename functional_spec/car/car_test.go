package car

import (
	"reflect"
	"testing"
)

func TestCreateCar(t *testing.T) {
	type args struct {
		plateNumber string
		color       string
	}
	tests := []struct {
		name string
		args args
		want *Car
	}{
		{
			"Test 1",
			args{plateNumber: "BE4508GE", color: "White"},
			&Car{PlateNumber: "BE4508GE", Color: "White"},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := CreateCar(tt.args.plateNumber, tt.args.color); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CreateCar() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCar_IsEqual(t *testing.T) {
	type fields struct {
		Number string
		Color  string
	}
	type args struct {
		cr Car
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			"Testcase 1: Test equal color. Test same color.",
			fields{
				Number: "BE4508GE",
				Color:  "Red",
			},
			args{
				cr: Car{PlateNumber: "BE4508GE", Color: "Red"},
			},
			true,
		},
		{
			"Testcase 2: Test equal color. Test different color.",
			fields{
				Number: "BE4508GE",
				Color:  "Red",
			},
			args{
				cr: Car{PlateNumber: "BE4508GE", Color: "Blue"},
			},
			false,
		},
		{
			"Testcase 3: Test equal color. Test different color.",
			fields{
				Number: "BE4508GE",
				Color:  "Red",
			},
			args{
				cr: Car{PlateNumber: "BE4508GE", Color: "red"},
			},
			true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			this := &Car{
				PlateNumber: tt.fields.Number,
				Color:       tt.fields.Color,
			}
			if got := this.IsEqual(tt.args.cr); got != tt.want {
				t.Errorf("Car.IsEqual() = %v, want %v", got, tt.want)
			}
		})
	}
}
