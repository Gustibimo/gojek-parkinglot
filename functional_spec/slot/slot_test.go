package slot

import (
	"testing"

	"gitlab.com/Gustibimo/gojek-parkinglot/functional_spec/car"
)

func TestSlot_Allocate(t *testing.T) {
	type fields struct {
		Index uint
		Car   *car.Car
	}
	type args struct {
		c car.Car
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			"Test 1: Slot already allocated",
			fields{Index: 2, Car: &car.Car{PlateNumber: "BE4508GE", Color: "Red"}},
			args{c: car.Car{PlateNumber: "BE4508GE", Color: "Red"}},
			true,
		},

		{
			"Test 2: Slot available",
			fields{Index: 2, Car: nil},
			args{c: car.Car{PlateNumber: "BE4508GE", Color: "Red"}},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &Slot{
				Index: tt.fields.Index,
				Car:   tt.fields.Car,
			}
			if err := s.Allocate(tt.args.c); (err != nil) != tt.wantErr {
				t.Errorf("Slot.Allocate() error = %v , wantErr %v", err, tt.wantErr)
			}

		})
	}
}

func TestSlot_Leave(t *testing.T) {
	type fields struct {
		Index uint
		Car   *car.Car
	}
	tests := []struct {
		name   string
		fields fields
	}{
		{
			"Test 1: Slot is already empty",
			fields{Index: 1, Car: &car.Car{PlateNumber: "BE4508GE", Color: "Red"}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &Slot{
				Index: tt.fields.Index,
				Car:   tt.fields.Car,
			}
			s.Leave()
			if s.Car != nil {
				t.Errorf("Slot.Leave() car is not nil, want nil")
			}
		})
	}
}

func TestSlot_IsFree(t *testing.T) {
	type fields struct {
		Index uint
		Car   *car.Car
	}
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &Slot{
				Index: tt.fields.Index,
				Car:   tt.fields.Car,
			}
			if got := s.IsFree(); got != tt.want {
				t.Errorf("Slot.IsFree() = %v, want %v", got, tt.want)
			}
		})
	}
}
