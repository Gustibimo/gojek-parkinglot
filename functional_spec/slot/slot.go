package slot

import (
	"fmt"

	"gitlab.com/Gustibimo/gojek-parkinglot/functional_spec/car"
)

type Slot struct {
	Index uint
	Car   *car.Car
}

func CreateSlot() *Slot {
	return &Slot{}
}

func (s *Slot) Allocate(c car.Car) error {
	if s.Car != nil {
		return fmt.Errorf("slot: Slot is already occupied")
	}
	s.Car = &c
	return nil
}

func (s *Slot) GetCarPlate() string {
	return s.Car.PlateNumber

}

func (s *Slot) GetCarColor() string {
	return s.Car.Color

}

func (s *Slot) Leave() {
	s.Car = nil
}

func (s *Slot) IsFree() bool {
	return s.Car == nil
}
