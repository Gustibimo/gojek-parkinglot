package cmd

import (
	"fmt"
	"strings"

	"gitlab.com/Gustibimo/gojek-parkinglot/functional_spec/parking"
)

type SlotNumCarColor struct {
	Command
	CarColor string
}

func (cmd *SlotNumCarColor) ParseArgs(args string) error {
	cmd.Args = strings.Split(args, " ")
	if !cmd.ValidateParams() {
		return errInvalidParam
	}
	cmd.CarColor = cmd.Args[0]
	return nil
}

func (cmd *SlotNumCarColor) Clear() {
	cmd.Args = nil
	cmd.CarColor = ""
}

func (cmd *SlotNumCarColor) ValidateParams() bool {
	return len(cmd.Args) == 1 && cmd.Args[0] != ""
}

func (cmd *SlotNumCarColor) Run() (string, error) {
	var output string
	var list []string
	slots := parking.GetParking().GetSlotsByCarColor(cmd.CarColor)
	if slots == nil {
		return "Not found", nil
	}
	for _, s := range slots {
		list = append(list, fmt.Sprint(s.Index))
	}
	output = strings.Join(list, ", ")
	return output, nil
}
