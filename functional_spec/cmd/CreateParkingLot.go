package cmd

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/Gustibimo/gojek-parkinglot/functional_spec/parking"
)

type CreateParkingLot struct {
	Command
	Size uint
}

func (cmd *CreateParkingLot) ParseArgs(args string) error {
	cmd.Args = strings.Split(args, " ")
	if !cmd.ValidateParams() {
		return errInvalidParam
	}
	value, err := strconv.ParseUint(args, 0, 64)
	if err != nil {
		return errInvalidParam
	}
	cmd.Size = uint(value)
	return nil
}

func (cmd *CreateParkingLot) ValidateParams() bool {
	return len(cmd.Args) == 1
}

func (cmd *CreateParkingLot) Clear() {
	cmd.Args = nil
	cmd.Size = 0
}

func (cmd *CreateParkingLot) Run() (string, error) {
	var output string
	obj := parking.CreateParking(cmd.Size)
	if obj == nil {
		return output, fmt.Errorf("error")
	}
	output = fmt.Sprintf("created a parking lot with %d slots", cmd.Size)
	return output, nil
}
