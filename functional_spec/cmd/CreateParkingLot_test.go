package cmd

import "testing"

func TestCreateParkingLot_ParseArgs(t *testing.T) {
	type fields struct {
		Command Command
		Size    uint
	}
	type args struct {
		args string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			"Test 1: one argument in create_parking_lot 3 produce parking object with size=3",
			fields{Size: 3},
			args{args: "3"},
			false,
		},
		{
			"Test 2: input multiple arguments, is invalid and error",
			fields{Size: 3},
			args{args: "3 7"},
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cmd := &CreateParkingLot{
				Command: tt.fields.Command,
				Size:    tt.fields.Size,
			}
			if err := cmd.ParseArgs(tt.args.args); (err != nil) != tt.wantErr {
				t.Errorf("CreateParkingLot.ParseArgs() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestCommandCreateParkingLot_ValidateParams(t *testing.T) {
	type fields struct {
		Command Command
		Size    uint
	}
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			"Test 1: valid argument",
			fields{Size: 3, Command: Command{Args: []string{"3"}}},
			true,
		},
		{
			"Test 2: invalid argument",
			fields{Size: 3, Command: Command{Args: []string{"3", "7"}}},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cmd := &CreateParkingLot{
				Command: tt.fields.Command,
				Size:    tt.fields.Size,
			}
			if got := cmd.ValidateParams(); got != tt.want {
				t.Errorf("CreateParkingLot.ValidateParams() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCommandCreateParkingLot_Clear(t *testing.T) {
	type fields struct {
		Command Command
		Size    uint
	}
	tests := []struct {
		name   string
		fields fields
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cmd := &CreateParkingLot{
				Command: tt.fields.Command,
				Size:    tt.fields.Size,
			}
			cmd.Clear()
		})
	}
}

func TestCommandCreateParkingLot_Run(t *testing.T) {
	type fields struct {
		Command Command
		Size    uint
	}
	tests := []struct {
		name    string
		fields  fields
		want    string
		wantErr bool
	}{
		{
			"Test 1",
			fields{Size: 3},
			"created a parking lot with 3 slots",
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cmd := &CreateParkingLot{
				Command: tt.fields.Command,
				Size:    tt.fields.Size,
			}
			got, err := cmd.Run()
			if (err != nil) != tt.wantErr {
				t.Errorf("CreateParkingLot.Run() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("CreateParkingLot.Run() = %v, want %v", got, tt.want)
			}
		})
	}
}
