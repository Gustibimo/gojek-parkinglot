package cmd

import (
	"testing"

	"gitlab.com/Gustibimo/gojek-parkinglot/functional_spec/car"
)

func TestPark_ValidateParams(t *testing.T) {
	type fields struct {
		Command Command
		Car     *car.Car
	}
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			"Test 1: valid command with 2 argument",
			fields{Command: Command{Args: []string{"BE4508GE", "Red"}}},
			true,
		},
		{
			"Test 2: invalid input, need 2 argument input",
			fields{Command: Command{Args: []string{"BE4508GE", "Red", "blue"}}},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cmd := &Park{
				Command: tt.fields.Command,
				Car:     tt.fields.Car,
			}
			if got := cmd.ValidateParams(); got != tt.want {
				t.Errorf("Park.ValidateParams() = %v, want %v", got, tt.want)
			}
		})
	}
}
