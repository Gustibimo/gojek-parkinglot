package cmd

import (
	"fmt"
	"strings"

	"gitlab.com/Gustibimo/gojek-parkinglot/functional_spec/parking"

	"gitlab.com/Gustibimo/gojek-parkinglot/functional_spec/car"
)

type Park struct {
	Command
	Car *car.Car
}

func (cmd *Park) ParseArgs(args string) error {
	cmd.Args = strings.Split(args, " ")
	if !cmd.ValidateParams() {
		return errInvalidParam
	}
	cmd.Car = car.CreateCar(cmd.Args[0], cmd.Args[1])
	return nil
}

func (cmd *Park) Clear() {
	cmd.Args = nil
	cmd.Car = nil
}

// command park require 2 argument, plate and color
func (cmd *Park) ValidateParams() bool {
	return len(cmd.Args) == 2
}

func (cmd *Park) Run() (string, error) {
	var output string
	s, err := parking.GetParking().AddCar(*cmd.Car)
	if err != nil {
		return output, err
	}
	output = fmt.Sprintf("Allocated slot number: %d", s.Index)
	return output, err
}
